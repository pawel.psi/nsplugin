import { Common } from './plugin2.common';
declare let LANProperties;

export class Plugin2 extends Common {
    readonly SSID_UNDEFINED = 'No WiFi Available';

    constructor() {
        super();
    }

    getSSID(): string {
        return this.getWifiSSID();
    }

    private getWifiSSID(): string {
        const ssid = LANProperties.fetchSSIDInfo();

        return ssid && ssid !== this.SSID_UNDEFINED ? ssid : '';
    }
}
