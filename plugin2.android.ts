import { Common } from './plugin2.common';
import * as application from 'tns-core-modules/application';
import Context = android.content.Context;
import WifiManager = android.net.wifi.WifiManager;
import WifiConfiguration = android.net.wifi.WifiConfiguration;
import List = java.util.List;


export class Plugin2 extends Common {
    
    getSSID(): string {
        application.android.registerBroadcastReceiver(android.content.Intent.ACTION_BATTERY_CHANGED, (context, intent) => {
            const level = intent.getIntExtra(android.os.BatteryManager.EXTRA_LEVEL, -1);
            const scale = intent.getIntExtra(android.os.BatteryManager.EXTRA_SCALE, -1);
            const percent = (level / scale) * 100.0;
            console.log('battery:', percent.toString());
        });
        return this.getWifiSSID();
        
    }
    
    private getWifiSSID(): string {
        const wifiManager: WifiManager = application.android.context.getSystemService(Context.WIFI_SERVICE);
        const wifiInfo = wifiManager.getConnectionInfo();
        console.log(wifiInfo);
        const listOfConfigurations: List<WifiConfiguration> = wifiManager.getConfiguredNetworks();
        let index;

        for (index = 0; index < listOfConfigurations.size(); index++) {
            const configuration: WifiConfiguration = listOfConfigurations.get(index);
            if (configuration.networkId == wifiInfo.getNetworkId()) {
               // console.log(configuration);
            }
        }

        return '';
    }

    private unquoteText(value: string): string {
        return (value != null || value != undefined) ? value.replace(/(^")|("$)/g, '') : value;
    }


}
